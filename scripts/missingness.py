#!/usr/bin/env python3
import sys
import gzip
import multiprocessing as mp
from multiprocessing import Pool


def file_missingness(filename: str):
    n_missing = 0
    n_samples = 0
    header_idx = 0

    delimiter = '\t'
    opener = open
    missing_chars = ['N', 'NN', 'NNN', 'NNNN', '-', '0']
    comment_char = '#'
    header_str = 'rs#'

    if filename.endswith('.gz'):
        opener = gzip.open
        delimiter = bytes(delimiter, 'utf8')
        missing_chars = [bytes(x, 'utf8') for x in missing_chars]
        comment_char = bytes(comment_char, 'utf8')
        header_str = bytes(header_str, 'utf8')

    with opener(filename) as f:
        for idx, line in enumerate(f):
            if line.startswith(comment_char):
                continue
            if line.startswith(header_str):
                header_idx = idx
                continue
            if idx > header_idx + 1:
                n_missing += sum(x in missing_chars for x in line.split(delimiter)[12:])
            else:
                fields = line.split(delimiter)[12:]
                n_samples = len(fields)
                n_missing += sum(x in missing_chars for x in fields)

    n_markers = idx - header_idx
    n_obs = n_samples * n_markers
    p_missing = n_missing / n_obs
    # print(filename, n_markers, n_samples, n_obs, n_missing, f'{p_missing:.2f}', sep='\t')
    return filename, n_markers, n_samples, n_obs, n_missing, p_missing


def main():
    with Pool() as pool:
        stats = pool.map(file_missingness, sys.argv[1:])
        print("# file", "n_markers", "n_samples", "n_obs", "n_missing", "p_missing", sep="\t")
        for filename, n_markers, n_samples, n_obs, n_missing, p_missing in stats:
            print(filename.split('/')[-1], n_markers, n_samples, n_obs, n_missing, f'{p_missing:.2f}', sep='\t')


if __name__ == '__main__':
    mp.set_start_method('spawn')
    main()
